// 1. 引入模块
const http = require('http')
const db = require('./db02.js')
const heros = require('./model.js')
const mongoose = require('mongoose')

// 2. 创建服务器
const app = http.createServer();

// 4. 注册监听事件
app.on('request', (req, res) => {
    // 设置响应头
    res.setHeader('content-type', 'text/html;charset=utf8');

    // 判断是否请求根路径
    if(req.url === '/') {
        // 如果是中文，会乱码
        // res.end('陈伟');
        db(() => {
            // 意味着数据库连接成功的
            const sel = {name: 1, age: 1, _id: 0}
            heros.find().select(sel).exec((err, data) => {
                if(err) throw err;

                // JSON.stringify() 方法把数据变成字符串返回
                res.end(JSON.stringify(data));
                mongoose.connection.close();
            })
        })
    } else if(req.url === '/data') {
        res.end('其他的查询方式')
    }
})

// 3. 挂起并监听
app.listen(3000, () => {
    console.log('服务器启动了，运行在 http://127.0.0.1:3000');
})
// 1. 引入模块
const mongoose = require('mongoose');

// 2. 创建集合规范 - 集合中每一条数据的字段
const herosSchema = mongoose.Schema({
    name: String,
    age: Number,
    skill: Array,
    isSkin: Boolean,
    createTime: Date
})

// 3. 创建操作集合的模型对象
// 参数1：在数据库中产生集合的名称   参数2：采用哪种方式规范集合的字段

// 导出创建的模型对象
module.exports = mongoose.model('heros', herosSchema)